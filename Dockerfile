FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["Alice/Alice.csproj", "Alice/"]
RUN dotnet restore "Alice/Alice.csproj"
COPY . .
WORKDIR "/src/Alice"
RUN dotnet build "Alice.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Alice.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Alice.dll"]