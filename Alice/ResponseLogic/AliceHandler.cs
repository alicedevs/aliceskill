using Alice.ResponseLogic.SpecialHandlers;
using Alice.Schedule;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Yandex.Alice.Sdk.Models;

namespace Alice.ResponseLogic
{
    public class AliceHandler
    {
        string name = "Лапутина Дарья Кирилловна";
        bool isAsked = false;
        public string Process(AliceRequest aliceRequest)
        {
            Request rq = new Request();
            var cultureInfo = new CultureInfo("ru-RU");

            if (aliceRequest.Session.MessageId == 0)
            {
                return "Здравствуйте! Чтобы получить информацию о расписании, нужно назвать своё имя. Как Вас зовут?";
            }

            if (aliceRequest.Session.MessageId == 1)
            {
                return "Приятно познакомиться! Что бы Вы хотели узнать?";

            }

            string request = aliceRequest.Request.Command.ToLower();

            if (request.Contains("расписание на"))
            {
                if (aliceRequest.Request.Command.Contains("понедельник"))
                {
                    return rq.scheduleForDay("Понедельник", name);
                }

                else if (request.Contains("вторник"))
                {
                    return rq.scheduleForDay("Вторник", name);
                }

                else if (request.Contains("среду"))
                {
                    return rq.scheduleForDay("Среда", name);
                }

                else if (request.Contains("четверг"))
                {
                    return rq.scheduleForDay("Четверг", name);
                }

                else if (request.Contains("пятницу"))
                {
                    return rq.scheduleForDay("Пятница", name);
                }

                else if (request.Contains("субботу"))
                {
                    return rq.scheduleForDay("Суббота", name);
                }

                else if (request.Contains("сегодня"))
                {
                    DateTime currentDay = DateTime.Today;
                    return rq.scheduleForDay(currentDay.ToString("dddd", cultureInfo), name);
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    DateTime currentDay = DateTime.Today;
                    return rq.scheduleForDay(currentDay.AddDays(1).ToString("dddd", cultureInfo), name);
                }

                else if (request.Contains("после"))
                {
                    DateTime currentDay = DateTime.Today;
                    return rq.scheduleForDay(currentDay.AddDays(2).ToString("dddd", cultureInfo), name);
                }

                else
                {
                    string date = request.Substring(request.Length - 5, 5);
                    var dateTime = DateTime.Parse(date, cultureInfo);
                    return rq.scheduleForDay(dateTime.ToString("dddd", cultureInfo), name);
                }
            }

            else if (request.Contains("звонков"))
            {
                return rq.schedule();
            }

            else if (request.Contains("что"))
            {
                if (request.Contains("сегодня"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.ToString("dddd", cultureInfo);

                    switch (aliceRequest.Request.Command.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.scheduleForOneLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.scheduleForOneLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.scheduleForOneLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.scheduleForOneLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.scheduleForOneLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.scheduleForOneLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.scheduleForOneLesson(dayOfWeek, "7", name);
                    }
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(1).ToString("dddd", cultureInfo);

                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.scheduleForOneLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.scheduleForOneLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.scheduleForOneLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.scheduleForOneLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.scheduleForOneLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.scheduleForOneLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.scheduleForOneLesson(dayOfWeek, "7", name);
                    }
                }

                else if (request.Contains("после"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(2).ToString("dddd", cultureInfo);

                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.scheduleForOneLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.scheduleForOneLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.scheduleForOneLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.scheduleForOneLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.scheduleForOneLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.scheduleForOneLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.scheduleForOneLesson(dayOfWeek, "7", name);
                    }
                }
            }

            else if (request.StartsWith("в какой аудитории"))
            {
                if (request.Contains("понедельник"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("понедельник", "1", name);

                        case "2":
                            return rq.roomForLesson("понедельник", "2", name);

                        case "3":
                            return rq.roomForLesson("понедельник", "3", name);

                        case "4":
                            return rq.roomForLesson("понедельник", "4", name);

                        case "5":
                            return rq.roomForLesson("понедельник", "5", name);

                        case "6":
                            return rq.roomForLesson("понедельник", "6", name);

                        case "7":
                            return rq.roomForLesson("понедельник", "7", name);
                    }
                }

                else if (request.Contains("вторник"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("вторник", "1", name);

                        case "2":
                            return rq.roomForLesson("вторник", "2", name);

                        case "3":
                            return rq.roomForLesson("вторник", "3", name);

                        case "4":
                            return rq.roomForLesson("вторник", "4", name);

                        case "5":
                            return rq.roomForLesson("вторник", "5", name);

                        case "6":
                            return rq.roomForLesson("вторник", "6", name);

                        case "7":
                            return rq.roomForLesson("вторник", "7", name);
                    }
                }

                else if (request.Contains("среду"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("среда", "1", name);

                        case "2":
                            return rq.roomForLesson("среда", "2", name);

                        case "3":
                            return rq.roomForLesson("среда", "3", name);

                        case "4":
                            return rq.roomForLesson("среда", "4", name);

                        case "5":
                            return rq.roomForLesson("среда", "5", name);

                        case "6":
                            return rq.roomForLesson("среда", "6", name);

                        case "7":
                            return rq.roomForLesson("среда", "7", name);
                    }
                }

                else if (request.Contains("четверг"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("четверг", "1", name);

                        case "2":
                            return rq.roomForLesson("четверг", "2", name);

                        case "3":
                            return rq.roomForLesson("четверг", "3", name);

                        case "4":
                            return rq.roomForLesson("четверг", "4", name);

                        case "5":
                            return rq.roomForLesson("четверг", "5", name);

                        case "6":
                            return rq.roomForLesson("четверг", "6", name);

                        case "7":
                            return rq.roomForLesson("четверг", "7", name);
                    }
                }

                else if (request.Contains("пятницу"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("пятница", "1", name);

                        case "2":
                            return rq.roomForLesson("пятница", "2", name);

                        case "3":
                            return rq.roomForLesson("пятница", "3", name);

                        case "4":
                            return rq.roomForLesson("пятница", "4", name);

                        case "5":
                            return rq.roomForLesson("пятница", "5", name);

                        case "6":
                            return rq.roomForLesson("пятница", "6", name);

                        case "7":
                            return rq.roomForLesson("пятница", "7", name);
                    }
                }

                else if (request.Contains("субботу"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson("суббота", "1", name);

                        case "2":
                            return rq.roomForLesson("суббота", "2", name);

                        case "3":
                            return rq.roomForLesson("суббота", "3", name);

                        case "4":
                            return rq.roomForLesson("суббота", "4", name);

                        case "5":
                            return rq.roomForLesson("суббота", "5", name);

                        case "6":
                            return rq.roomForLesson("суббота", "6", name);

                        case "7":
                            return rq.roomForLesson("суббота", "7", name);
                    }
                }

                else if (request.Contains("сегодня"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.ToString("dddd", cultureInfo);

                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.roomForLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.roomForLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.roomForLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.roomForLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.roomForLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.roomForLesson(dayOfWeek, "7", name);
                    }
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(1).ToString("dddd", cultureInfo);

                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.roomForLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.roomForLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.roomForLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.roomForLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.roomForLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.roomForLesson(dayOfWeek, "7", name);
                    }
                }

                else if (request.Contains("после"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(2).ToString("dddd", cultureInfo);

                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.roomForLesson(dayOfWeek, "1", name);

                        case "2":
                            return rq.roomForLesson(dayOfWeek, "2", name);

                        case "3":
                            return rq.roomForLesson(dayOfWeek, "3", name);

                        case "4":
                            return rq.roomForLesson(dayOfWeek, "4", name);

                        case "5":
                            return rq.roomForLesson(dayOfWeek, "5", name);

                        case "6":
                            return rq.roomForLesson(dayOfWeek, "6", name);

                        case "7":
                            return rq.roomForLesson(dayOfWeek, "7", name);
                    }
                }
            }

            else if (request.StartsWith("сколько"))
            {
                if (request.Contains("пар"))
                {
                    if (request.Contains("понедельник"))
                    {
                        return rq.quantityOfLessonsDay("понедельник", name);
                    }

                    else if (request.Contains("вторник"))
                    {
                        return rq.quantityOfLessonsDay("вторник", name);
                    }

                    else if (request.Contains("среду"))
                    {
                        return rq.quantityOfLessonsDay("среда", name);
                    }

                    else if (request.Contains("четверг"))
                    {
                        return rq.quantityOfLessonsDay("четверг", name);
                    }

                    else if (request.Contains("пятницу"))
                    {
                        return rq.quantityOfLessonsDay("пятница", name);
                    }

                    else if (request.Contains("субботу"))
                    {
                        return rq.quantityOfLessonsDay("суббота", name);
                    }

                    else if (request.Contains("сегодня"))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.ToString("dddd", cultureInfo);

                        return rq.quantityOfLessonsDay(dayOfWeek, name);
                    }

                    else if (request.Contains("завтра") & !(request.Contains("после")))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.AddDays(1).ToString("dddd", cultureInfo);

                        return rq.quantityOfLessonsDay(dayOfWeek, name);
                    }

                    else if (request.Contains("после"))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.AddDays(2).ToString("dddd", cultureInfo);

                        return rq.quantityOfLessonsDay(dayOfWeek, name);
                    }

                    else if (request.Contains("неделе"))
                    {
                        switch (request.Substring(request.Length - 10, 2))
                        {
                            case " 1":
                                return rq.quantityLessonsWeek(1, name);

                            case " 2":
                                return rq.quantityLessonsWeek(2, name);

                            case " 3":
                                return rq.quantityLessonsWeek(3, name);

                            case " 4":
                                return rq.quantityLessonsWeek(4, name);

                            case " 5":
                                return rq.quantityLessonsWeek(5, name);

                            case " 6":
                                return rq.quantityLessonsWeek(6, name);

                            case " 7":
                                return rq.quantityLessonsWeek(7, name);

                            case " 8":
                                return rq.quantityLessonsWeek(8, name);

                            case " 9":
                                return rq.quantityLessonsWeek(9, name);

                            case "10":
                                return rq.quantityLessonsWeek(10, name);

                            case "11":
                                return rq.quantityLessonsWeek(11, name);

                            case "12":
                                return rq.quantityLessonsWeek(12, name);

                            case "13":
                                return rq.quantityLessonsWeek(13, name);

                            case "14":
                                return rq.quantityLessonsWeek(14, name);

                            case "15":
                                return rq.quantityLessonsWeek(15, name);

                            case "16":
                                return rq.quantityLessonsWeek(16, name);
                        }
                    }
                }

                if (request.Contains("окон"))
                {
                    if (request.Contains("понедельник"))
                    {
                        return rq.windowScheduleWeek("понедельник", name);
                    }

                    else if (request.Contains("вторник"))
                    {
                        return rq.windowScheduleWeek("вторник", name);
                    }

                    else if (request.Contains("среду"))
                    {
                        return rq.windowScheduleWeek("среда", name);
                    }

                    else if (request.Contains("четверг"))
                    {
                        return rq.windowScheduleWeek("четверг", name);
                    }

                    else if (request.Contains("пятницу"))
                    {
                        return rq.windowScheduleWeek("пятница", name);
                    }

                    else if (request.Contains("субботу"))
                    {
                        return rq.windowScheduleWeek("суббота", name);
                    }

                    else if (request.Contains("сегодня"))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.ToString("dddd", cultureInfo);

                        return rq.windowScheduleWeek(dayOfWeek, name);
                    }

                    else if (request.Contains("завтра") & !(aliceRequest.Request.Command.Contains("после")))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.AddDays(1).ToString("dddd", cultureInfo);

                        return rq.windowScheduleWeek(dayOfWeek, name);
                    }

                    else if (request.Contains("после"))
                    {
                        DateTime currentDay = DateTime.Today;
                        string dayOfWeek = currentDay.AddDays(2).ToString("dddd", cultureInfo);

                        return rq.windowScheduleWeek(dayOfWeek, name);
                    }
                }
            }

            else if (request.StartsWith("к какой паре"))
            {
                if (request.Contains("понедельник"))
                {
                    return rq.firstLesson("понедельник", name);
                }

                else if (request.Contains("вторник"))
                {
                    return rq.firstLesson("вторник", name);
                }

                else if (request.Contains("среду"))
                {
                    return rq.firstLesson("среда", name);
                }

                else if (request.Contains("четверг"))
                {
                    return rq.firstLesson("четверг", name);
                }

                else if (request.Contains("пятницу"))
                {
                    return rq.firstLesson("пятница", name);
                }

                else if (request.Contains("субботу"))
                {
                    return rq.firstLesson("суббота", name);
                }

                else if (request.Contains("сегодня"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.ToString("dddd", cultureInfo);

                    return rq.firstLesson(dayOfWeek, name);
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(1).ToString("dddd", cultureInfo);

                    return rq.firstLesson(dayOfWeek, name);
                }

                else if (request.Contains("после"))
                {
                    DateTime currentDay = DateTime.Today;
                    string dayOfWeek = currentDay.AddDays(2).ToString("dddd", cultureInfo);

                    return rq.firstLesson(dayOfWeek, name);
                }
            }

            else if (request.Contains("начинается") || request.Contains("начинаются"))
            {
                if (request.Contains("понедельник"))
                {
                    return "В понедельник пары начинаются в 14:20";
                }

                else if (request.Contains("вторник"))
                {
                    return "Во вторник пары начинаются в 10:40";
                }

                else if (request.Contains("среду"))
                {
                    return "В среду пары начинаются в 9:00";
                }

                else if (request.Contains("четверг"))
                {
                    return "В четверг пары начинаются в 9:00";
                }

                else if (request.Contains("пятницу"))
                {
                    return "В пятницу пары начинаются в 9:00";
                }

                else if (request.Contains("субботу"))
                {
                    return "В субботу пары начинаются в 9:00";
                }

                else if (request.Contains("сегодня"))
                {
                    return "Сегодня пары начинаются в 9:00";
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    return "Завтра пары начинаются в 9:00";
                }

                else if (request.Contains("после"))
                {
                    return "Послезавтра пары начинаются в 9:00";
                }

                else if (request.Contains("пара"))
                {
                    switch (request.Substring(request.Length - 10, 1))
                    {
                        case "1":
                            return rq.breakStart("1");

                        case "2":
                            return rq.breakStart("2");

                        case "3":
                            return rq.breakStart("3");

                        case "4":
                            return rq.breakStart("4");

                        case "5":
                            return rq.breakStart("5");

                        case "6":
                            return rq.breakStart("6");

                        case "7":
                            return rq.breakStart("7");
                    }
                }
            }

            else if (request.Contains("заканчивается") || request.Contains("заканчиваются"))
            {
                if (request.Contains("понедельник"))
                {
                    return "В понедельник пары заканчиваются в 17:50";
                }

                else if (request.Contains("вторник"))
                {
                    return "Во вторник пары заканчиваются в 19:30";
                }

                else if (request.Contains("среду"))
                {
                    return "В среду пары заканчиваются в 17:50";
                }

                else if (request.Contains("четверг"))
                {
                    return "В четверг пары заканчиваются в 10:30";
                }

                else if (request.Contains("пятницу"))
                {
                    return "В пятницу пары заканчиваются в 10:30";
                }

                else if (request.Contains("субботу"))
                {
                    return "В субботу пары заканчиваются в 12:40";
                }

                else if (request.Contains("сегодня"))
                {
                    return "Сегодня пары заканчиваются в 10:30";
                }

                else if (request.Contains("завтра") & !(request.Contains("после")))
                {
                    return "Завтра пары заканчиваются в 10:30";
                }

                else if (request.Contains("после"))
                {
                    return "Послезавтра пары заканчиваются в 10:30";
                }

                else if (request.Contains("пара"))
                {
                    switch (request.Substring(request.Length - 7, 1))
                    {
                        case "1":
                            return rq.breakFinish("1");

                        case "2":
                            return rq.breakFinish("2");

                        case "3":
                            return rq.breakFinish("3");

                        case "4":
                            return rq.breakFinish("4");

                        case "5":
                            return rq.breakFinish("5");

                        case "6":
                            return rq.breakFinish("6");

                        case "7":
                            return rq.breakFinish("7");
                    }
                }
            }

            else if (request.StartsWith("начало"))
            {
                switch (request.Substring(request.Length - 6, 1))
                {
                    case "1":
                        return rq.breakStartAndFinish("1");

                    case "2":
                        return rq.breakStartAndFinish("2");

                    case "3":
                        return rq.breakStartAndFinish("3");

                    case "4":
                        return rq.breakStartAndFinish("4");

                    case "5":
                        return rq.breakStartAndFinish("5");

                    case "6":
                        return rq.breakStartAndFinish("6");

                    case "7":
                        return rq.breakStartAndFinish("7");
                }
            }
            
            return "Команда не распознана! Пожалуйста, повторите запрос.";
            //return HelloHandler.Process();
        }

    }
}




