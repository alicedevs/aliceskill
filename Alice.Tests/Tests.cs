using NUnit.Framework;
using Alice.ResponseLogic;
using Yandex.Alice.Sdk.Models;
using System;

namespace Alice.Tests
{
    public class Tests
    {
        AliceHandler handler { get; set; }
        AliceRequest request { get; set; }

        [SetUp]
        public void Setup()
        {
            handler = new AliceHandler();
            request = new AliceRequest()
            {
                Request = new AliceRequestModel<object>()
                {
                    Command = new String("")
                },
                Session = new AliceSessionModel()
                {
                    MessageId = 2
                }
            };
        }

        bool CheckRequest(string input, string output, out string response)
        {
            request.Request.Command = input;
            response = handler.Process(request);

            return (response == output) ? true : false;
        }

        [Test]
        public void LessonTimeRequestTest() // все норм
        {
            if (CheckRequest("Во сколько сегодня начинаются пары?",
                "Сегодня пары начинаются в 9:00",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void DayOfTheWeekRequestTest1() //все норм
        {
            if (CheckRequest("Расписание на понедельник",
                "4 Мат. анализ 5 Введ. в проф. деят.",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void DayOfTheWeekRequestTest2() //все норм
        {
            if (CheckRequest("Расписание на среду",
                "1 ТСПП 4 Англ. язык 5 Дифф. уравнения",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void DayOfTheWeekRequestTest3() //все норм
        {
            if (CheckRequest("Какое расписание на среду",
                "1 ТСПП 4 Англ. язык 5 Дифф. уравнения",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void TodayTest() //все норм
        {
            if (CheckRequest("Расписание на сегодня",
                "1 Архитектура компьютеров",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void TomorrowTest() //все норм
        {
            if (CheckRequest("Расписание на завтра",
                "1 Мет. компл. анализа",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void NumberOfLessonTest1() //все норм
        {
            if (CheckRequest("Что сегодня на 1 паре?",
                "Архитектура компьютеров",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void NumberOfLessonTest2() //все норм
        {
            if (CheckRequest("Что завтра на 1 паре?",
                "Мет. компл. анализа",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void NumberOfLessonTest3() //чет не так
        {
            if (CheckRequest("Что завтра на 4 паре?",
                "На этой паре окно",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void LessonByDayRequestTest1() //все норм
        {
            if (CheckRequest("Расписание на 09.06",
                "1 ТСПП 4 Англ. язык 5 Дифф. уравнения",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void LessonByDayRequestTest2() //все норм
        {
            if (CheckRequest("Расписание на 10.06",
                "1 Архитектура компьютеров",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void ClassroomTest1() //все норм
        {
            if (CheckRequest("В какой аудитории сегодня 1 пара?",
                "Дистанционная пара",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void ClassroomTest2() //все норм
        {
            if (CheckRequest("В какой аудитории сегодня 3 пара?",
                "На этой паре окно",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void ClassroomTest3() //все норм
        {
            if (CheckRequest("В какой аудитории в среду 1 пара?",
                "Б-209",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void QuantutyOfLessonsOnDayTest1() //все норм
        {
            if (CheckRequest("Сколько пар в понедельник?",
                "2",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void QuantutyOfLessonsOnDayTest2() //все норм
        {
            if (CheckRequest("Сколько пар в среду?",
                "3",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void QuantutyOfLessonsOnWeekTest1() // тест должен не проходить
        {
            if (CheckRequest("Сколько пар на 5 неделе?",
                "3",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void TimeOfStartOfTheDayTest1() //все норм
        {
            if (CheckRequest("Во сколько начинаются пары в понедельник?",
                "В понедельник пары начинаются в 14:20",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void TimeOfStartOfTheDayTest2() //все норм
        {
            if (CheckRequest("Во сколько начинаются пары в среду?",
                "В среду пары начинаются в 9:00",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void FirstLessonTest1() //все норм
        {
            if (CheckRequest("К какой паре в понедельник?",
                "4",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void FirstLessonTest2() //все норм
        {
            if (CheckRequest("К какой паре в среду?",
                "1",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void WindowTest1() // нет метода
        {
            if (CheckRequest("Сколько окон в среду?",
                "2",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void ScheduleTest1()
        {
            if (CheckRequest("Во сколько начинается 1 пара?",
                "1 пара начинается в 9:00",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

        [Test]
        public void ScheduleTest2()
        {
            if (CheckRequest("Начало и окончание 3 пары",
                "3 пара начинается в  12:40 и заканчивается в 14:10",
                out string response))
            {
                Assert.Pass();
            }
            else
            {
                Assert.Fail($"Wrong answer: {response}");
            }
        }

    }
}
