# How to configure server?

Update your package lists and install Docker, Nginx and MySQL.

## Get a Certificate

You will need to purchase or create an SSL certificate. These commands are for a self-signed certificate, but you should **get an officially signed certificate if you want to use this code as Yandex Alice skill**.

```
cd /etc/nginx
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/cert.key -out /etc/nginx/cert.crt
```

## Edit the Nginx Configuration 
Next you will need to edit the `default.conf` Nginx configuration file. You should edit the `nginx.conf` file in your server, appending this text in `server{}` part:

```
include /etc/nginx/default.d/*.conf;
```

The `default.conf` file:

```
server {
    listen 80;
    return 301 https://$host$request_uri;
}

server {

    listen 443;
    listen [::]:443;

    ssl_certificate        /etc/nginx/cert.crt;
    ssl_certificate_key    /etc/nginx/cert.key;

    ssl on;

    access_log            /var/log/nginx/jenkins.access.log;

    location / {

      proxy_set_header    Host $host;
      proxy_set_header    X-Real-IP $remote_addr;
      proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header    X-Forwarded-Proto $scheme;

      proxy_pass          http://localhost:8080;
      proxy_read_timeout  90;

      proxy_redirect      off;
    }
  }   
```
## Gitlab CI files

Also you need to change information in the `.gitlab-ci.yml` file for ssh connection.

## MySQL
Update the `my.cnf` file in your server, appending a public IP after `[client-server]`:

```
bind_address = XXX.XXX.XXX.XXX
```